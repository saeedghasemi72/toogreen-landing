import React from "react";
import { ThemeProvider } from "styled-components";

import dark from "./colors/dark";
import light from "./colors/light";
import common from "./common";

export default ({ children }) => {
  const colors = {
    dark,
    light
  };

  const theme = {
    color: colors.light,
    ...common
  };

  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};
