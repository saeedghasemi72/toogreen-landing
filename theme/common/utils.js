import { css } from "styled-components";

const flex = (jc = "center", ai = "center", fd = "row", fl = "flex") => css`
  display: ${fl};
  flex-direction: ${fd};
  justify-content: ${jc};
  align-items: ${ai};
`;

const title = (
  fs = 22,
  ff = "sahelBlack",
  color = "#c8ae7a",
  position = "right"
) => css`
  font-size: ${fs}px;
  font-weight: bold;
  position: relative;
  font-family: ${ff};
  ${position === "left"
    ? "text-align:left;padding-left: 60px;"
    : "text-align:right;"}

  ${ff === "sahelBlack" ? " padding-right: 90px;" : " padding-right: 60px;"}
  &:before {
    content: "";
    position: absolute;
    ${ff === "sahelBlack"
      ? "height: 7px;width: 68px;"
      : "height: 5px;width: 40px;"}

    background-color: ${color};
    ${position === "left" ? "left: 0;" : "right: 0;"}
    left: -25px;
    top: 0;
    bottom: 0;
    margin: auto 0;
  }
`;

export default {
  flex,
  title
};
