const screens = {
  // xs: "425px",
  sm: "576px",
  md: "768px",
  lg: "992px",
  xl: "1200px",
  xxl: "1600px"
  // xxxl: "1920px",
};

const fonts = {
  baloo: "Baloo Thambi 2",
  montserrat: "Montserrat"
};

export default {
  screens,
  fonts
};
