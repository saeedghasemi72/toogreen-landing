export default {
  primary: "#59AA47", //

  secondary: "#E06D15", //

  black: "#000000", //
  grayLight: "#838388", //
  text3: "#dadada",
  white: "#FFFFFF", //

  bgGreen: "rgba(89,170,71,0.05)",
  bgLightOrange: "rgba(242,126,36,0.07)",
  bg1: "#f9faf9",
  bgWhite: "#FFFFFF",

  success: "#007e33",
  danger: "#b71c1c"
};
