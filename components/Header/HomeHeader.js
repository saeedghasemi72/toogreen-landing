import { Row, Col } from "antd";
import { useTranslation } from "next-i18next";
import TopBar from "./TopBar";

import StyledWrapper from "./header.style";

const Header = ({ locale }) => {
  const { t } = useTranslation("header");

  return (
    <StyledWrapper locale={locale}>
      <div className="homeHeader">
        <div className="container">
          <TopBar />
          <Row>
            <Col xl={11}>
              <p className="slogan">{t("slogan")}</p>
              <p className="title">{t("title_line_1")}</p>
              <p className="title">{t("title_line_2")}</p>
              <p className="dec">{t("description")}</p>
              <div className="downloadWrapper">
                <a href="/" className="downloadBox">
                  <img src="/images/appstore.svg" alt="plate" />
                  <div>
                    <p>{t("download_on_the")}</p>
                    <p>{t("app_store")}</p>
                  </div>
                </a>
                <a href="/" className="downloadBox">
                  <img src="/images/googleplay.svg" alt="plate" />
                  <div>
                    <p>{t("download_on_the")}</p>
                    <p>{t("googlePlay")}</p>
                  </div>
                </a>
              </div>
              <img
                src="/images/homeHeader-pic2.png"
                alt="plate"
                className="md-none"
              />
            </Col>
            <Col xl={13}>
              <div className="imageWrapper homeBigImage xl-none">
                <img src="/images/homeHeader-pic.png" alt="toogreen" />
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </StyledWrapper>
  );
};

export default Header;
