import Link from "next/link";

import { Button } from "components";

import StyledWrapper from "./header.style";

const TopBar = () => {
  return (
    <StyledWrapper>
      <div className="topbar">
        <Link href="/">
          <a>
            <img src="/images/logo.svg" alt="logo" />
          </a>
        </Link>
        <Button variant="primary">Download App</Button>
      </div>
    </StyledWrapper>
  );
};

export default TopBar;
