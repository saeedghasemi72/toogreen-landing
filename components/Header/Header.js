import TopBar from "./TopBar";
import StyledWrapper from "./header.style";

const Header = ({ title }) => {
  return (
    <StyledWrapper>
      <div className="pageHeader">
        <div className="container">
          <TopBar />
          <h1>{title}</h1>
        </div>
      </div>
    </StyledWrapper>
  );
};

export default Header;
