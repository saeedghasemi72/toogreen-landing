import styled, { css } from "styled-components";
import { styleTranformHelper as t } from "../../helpers";

export default styled.header(
  ({
    theme: {
      color: { bgGreen, bgWhite, secondary, grayLight, black },
      utils: { flex },
      constant: { screens }
    }
  }) =>
    t(css`
      .topbar {
        ${flex("space-between", "center")};
        padding: 44px 0;
        button {
          font-family: "Baloo Thambi 2";
        }
      }
      .homeHeader {
        background-color: ${bgGreen};
        .slogan {
          font-size: 32px;
          color: ${secondary};
          margin-bottom: 5px;
          @media screen and (max-width: ${screens.md}) {
            font-size: 18px;
          }
        }
        .title {
          ${({ locale }) =>
            locale === "tr" ? " font-size: 40px;" : " font-size: 53px;"}
          font-weight: bold;
          margin: 0;
          line-height: 64px;
          @media screen and (max-width: ${screens.md}) {
            font-size: 36px;
          }
        }
        .dec {
          font-size: 18px;
          color: ${grayLight};
          margin: 24px 0;
        }
        .downloadWrapper {
          ${flex("flex-start", "center")};

          .downloadBox {
            padding: 12px 24px;
            ${flex("center", "center")};
            background-color: ${bgWhite};
            border-radius: 16px;
            @media screen and (max-width: ${screens.md}) {
              padding: 12px 20px;
              margin-bottom: 32px;
            }
            img {
              margin-right: 11px;
            }
            div {
              ${flex("center", "flex-start", "column")};

              p {
                margin: 0;
                color: ${black};
              }
              p:last-child {
                font-weight: bold;
                font-size: 16px;
              }
            }
          }
          .downloadBox:last-child {
            margin-left: 24px;
          }
        }
        .homeBigImage {
          height: 100%;
          display: flex;
          align-items: flex-end;
        }
      }
      .pageHeader {
        background-image: url("/images/pageHeaderBG.svg");
        background-size: cover;
        background-repeat: no-repeat;
        @media screen and (max-width: ${screens.sm}) {
          background-image: url("/images/pageHeaderBG-res.svg");
        }
        h1 {
          text-align: center;
          font-size: 56px;
          font-weight: bold;
          padding-top: 40px;
          padding-bottom: 80px;
          margin: 0;
          font-family: "Montserrat";
          line-height: 64px;
          @media screen and (max-width: ${screens.sm}) {
            line-height: 60px;
            padding-top: 50px;
            padding-bottom: 80px;
          }
        }
      }
    `)
);
