import React, { ReactNode, PureComponent } from "react";

import StyleWrapper from "./errorBoundary.style";

export default class ErrorBoundary extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { error: null, errorInfo: null };
  }

  componentDidCatch(error, errorInfo) {
    this.setState({ error, errorInfo });

    console.error("Error Boundry", error);
  }

  render() {
    const { children } = this.props;
    const { errorInfo, error } = this.state;

    return errorInfo ? (
      <StyleWrapper>
        <h2>Error happened!</h2>
        <details style={{ whiteSpace: "pre-wrap" }}>
          {error && error.toString()}
          <br />
          {errorInfo.componentStack}
        </details>
      </StyleWrapper>
    ) : (
      children || null
    );
  }
}
