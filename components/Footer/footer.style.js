import styled, { css } from "styled-components";
import { styleTranformHelper as t } from "../../helpers";

export default styled.footer(
  ({
    theme: {
      color: { grayLight, bgWhite, black },
      utils: { flex },
      constant: { screens }
    }
  }) =>
    t(css`
      margin-top: 120px;
      position: relative;
      font-family: "Baloo Thambi 2";
      &:before {
        content: "";
        position: absolute;
        width: 100%;
        height: 100%;
        background-image: url("/images/footer-bg.png");
        background-repeat: no-repeat;
        @media screen and (max-width: ${screens.sm}) {
          background-image: unset;
        }
      }
      .access {
        padding-top: 64px;
        padding-bottom: 32px;
      }
      .touch {
        padding-top: 64px;
        &__text {
          color: ${grayLight};
          font-size: 16px;
        }
        &__logo {
          img {
            min-width: 260px;
          }
          @media screen and (max-width: ${screens.md}) {
            height: 100%;
            ${flex("flex-start", "center")};
            img {
              min-width: 160px;
            }
          }
        }
      }
      .footerPicWrapper {
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: flex-end;
        img {
          width: 100%;
        }
      }
      .footer {
        /* background: rgba(240, 240, 245, 0.4);
        backdrop-filter: blur(8px); */
      }
      .downloadText {
        font-size: 16px;
        color: ${grayLight};
      }
      .downloadWrapper {
        ${flex("flex-start", "center")};
        .downloadBox {
          padding: 12px 24px;
          ${flex("flex-start", "center")};
          background-color: ${bgWhite};
          border-radius: 16px;
          width: 100%;
          img {
            margin-right: 11px;
          }
          div {
            ${flex("center", "flex-start", "column")};

            p {
              margin: 0;
              color: ${black};
            }
            p:last-child {
              font-weight: bold;
              font-size: 15px;
            }
          }
          @media screen and (max-width: ${screens.xxl}) {
            padding: 12px 12px;
          }
        }
        .downloadBox:last-child {
          margin-left: 24px;
          @media screen and (max-width: ${screens.md}) {
            margin-left: 16px;
          }
        }
        @media screen and (max-width: ${screens.md}) {
          padding: 12px 0;
          margin-bottom: 32px;
        }
      }
      .quickAccess {
        margin-top: 86px;
        @media screen and (min-width: ${screens.xxl}) {
          margin-top: 84px;
        }
        p {
          font-size: 16px;
          color: ${grayLight};
        }
        div {
          ${flex("flex-start", "center")};
          flex-wrap: wrap;
          a {
            margin-right: 32px;
            font-family: "Baloo Thambi 2";
            color: ${black};
            font-weight: 600;
            font-size: 16px;
          }
          .locale {
            background: white;
            padding: 5px 10px;
            border-radius: 3px;
            svg {
              margin-right: 3px;
            }
          }
        }
        @media screen and (max-width: ${screens.md}) {
          margin-top: 32px;
        }
      }
      .copyright {
        margin-top: 85px;
        font-size: 14px;
        color: ${grayLight};
        font-family: "Baloo Thambi 2";
        @media screen and (max-width: ${screens.md}) {
          margin-top: 32px;
        }
      }
      .social {
        margin: 48px 0;
        @media screen and (min-width: ${screens.md}) {
          padding-left: 24px;
        }
        a {
          margin-right: 34px;
        }
      }
      .contact {
        ${flex("flex-start", "center")};
        div {
          img {
            margin-right: 5px;
          }
        }
        div:first-child {
          margin-right: 83px;
        }
        span {
          font-weight: 600;
          font-size: 16px;
          color: ${black};
        }
      }
    `)
);
