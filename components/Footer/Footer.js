import { useTranslation } from "next-i18next";
import Link from "next/link";
import { Row, Col } from "antd";
import { GlobalOutlined } from "@ant-design/icons";
import StyledWrapper from "./footer.style";

const Footer = () => {
  const { t } = useTranslation("footer");
  return (
    <StyledWrapper>
      <div className="footer">
        <div className="container ">
          <Row
            gutter={[
              { xs: 0, md: 58 },
              { xs: 0, md: 0 }
            ]}
          >
            <Col xs={24} md={10}>
              <div className="touch">
                <Row
                  gutter={[
                    { xs: 24, md: 0 },
                    { xs: 24, md: 0 }
                  ]}
                >
                  <Col xs={12} md={24}>
                    <div className="touch__logo">
                      <Link href="/">
                        <a>
                          <img src="/images/logo.svg" alt="logo" />
                        </a>
                      </Link>
                    </div>
                  </Col>
                  <Col xs={12} md={24}>
                    <div className="social">
                      <a href="#">
                        <img src="/images/facebook.svg" alt="facebook" />
                      </a>
                      <a href="#">
                        <img src="/images/twitter.svg" alt="twitter" />
                      </a>
                      <a href="#">
                        <img src="/images/youtube.svg" alt="youtube" />
                      </a>
                    </div>
                  </Col>
                </Row>
                <p className="touch__text">{t("getInTouch")}</p>
                <div className="contact">
                  <div>
                    <img src="/images/email.svg" alt="youtube" />
                    <span>{t("email")}</span>
                  </div>
                  <div>
                    <img src="/images/telephone.svg" alt="youtube" />
                    <span>{t("phone")}</span>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={24} md={5}>
              <div className="footerPicWrapper md-none">
                <img src="/images/footer-pic.png" alt="fork" />
              </div>
            </Col>
            <Col xs={24} md={9}>
              <div className="access">
                <p className="downloadText">{t("downloadText")}</p>
                <div className="downloadWrapper">
                  <a href="/" className="downloadBox">
                    <img src="/images/appstore.svg" alt="appstore" />
                    <div>
                      <p>{t("download_on_the")}</p>
                      <p>{t("app_store")}</p>
                    </div>
                  </a>
                  <a href="/" className="downloadBox">
                    <img src="/images/googleplay.svg" alt="googleplay" />
                    <div>
                      <p>{t("download_on_the")}</p>
                      <p>{t("googlePlay")}</p>
                    </div>
                  </a>
                </div>
                <div className="quickAccess">
                  <p>{t("quickAccess")}</p>
                  <div>
                    <Link href="/help-center">
                      <a>{t("helpCenter")}</a>
                    </Link>
                    <Link href="/privacy">
                      <a>{t("privacyPolicy")}</a>
                    </Link>
                    <Link href="/terms">
                      <a>{t("terms")}</a>
                    </Link>
                    {/* <Link href="/en"> */}
                    <a href="/en" className="locale">
                      <GlobalOutlined />
                      English (EN)
                    </a>
                    {/* </Link> */}
                    {/* <Link href="/tr"> */}
                    <a href="/tr" className="locale">
                      <GlobalOutlined />
                      Turkish (TR)
                    </a>
                    {/* </Link> */}
                  </div>
                </div>
                <p className="copyright">{t("copyrights")}</p>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </StyledWrapper>
  );
};

export default Footer;
