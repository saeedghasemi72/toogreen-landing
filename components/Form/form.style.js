import styled, { css } from "styled-components";
import { styleTranformHelper as t } from "../../helpers";

export default styled.section(
  ({
    lg,
    theme: {
      color: {
        bgRed,
        primary900,
        light4,
        text4,
        textDark,
        dark3,
        secondaryRed,
        primary2
      }
    }
  }) =>
    t(css`
      &.input {
        margin-bottom: 16px;
        position: relative;
      }

      input:-webkit-autofill {
        background-color: red !important;
      }

      .ant-input {
        border: none;
        border-radius: 8px;
        height: 48px;
      }

      .input:-internal-autofill-selected {
        background-color: transparent;
      }

      label {
        color: ${dark3};
        font-weight: 300;
        font-size: 14px;
        /* margin-bottom: 8px; */
        display: block;
        line-height: 1;
      }

      .text-danger {
        /* position: absolute; */
        /* right: 17px; */
        /* bottom: -12px; */
        /* background-color: ${bgRed}; */
        padding: 2px 10px;
        border-radius: 8px;
        z-index: 1051;

        .text-danger--text {
          color: ${bgRed};
          font-size: 13px;
        }
      }

      /***************************** [ Select Input ] *****************************/
      &.select-input {
        .ant-select {
          height: 52px;
          width: 100%;

          .ant-select-selector {
            height: 52px;
            min-height: 52px;
            outline: none;
            box-shadow: none;
            border: none;
            border-radius: 8px;
            background-color: ${light4};
            .ant-select-selection-search-input {
              height: 52px;
            }

            .ant-select-selection-placeholder {
              line-height: 52px;
            }

            .ant-select-selection-item {
              line-height: 52px;
            }
          }
        }
      }

      /***************************** [ Checkbox Input ] **************************/
      &.checkbox-input {
        .ant-checkbox-wrapper {
          .ant-checkbox {
            display: inline-block;
            vertical-align: top;

            .ant-checkbox-inner {
              width: 18px;
              height: 18px;
              border-radius: 3px;
              border: 2px solid #008295;
              background-color: white;
            }
          }

          &.ant-checkbox-wrapper-checked {
            .ant-checkbox {
              .ant-checkbox-inner {
                background-color: #008295;
              }
            }
          }

          span {
            &:last-child {
              display: inline-block;
              width: 90%;
              vertical-align: top;
              line-height: 20px;
              color: ${textDark};
              font-size: 16px;
            }
          }
        }
      }
      /***************************** [ Checkbox Input ] **************************/
      &.submit-button {
        button {
          /* width: 100%; */
          /* background-color: ${primary900}; */
          border: none;
          outline: none;
          border-radius: 8px;
          color: ${text4};
          font-weight: 300;
          font-size: 12px;
          height: 48px;
          border-radius: 7px;
          background: #2c4836;
          ${lg &&
          `
          height: 50px;
        `}
        }
        button.secondary {
          background-color: ${secondaryRed};
        }
        .icon {
          max-width: 20px;
          margin-right: 7px;
          vertical-align: middle;
          display: inline;
          svg {
            max-width: inherit;
            vertical-align: middle;
            margin-bottom: 4px;
          }
        }
        span {
          vertical-align: middle;
          font-size: initial;
        }
      }
    `)
);

/** *************************** [ Button ] ************************* */

export const ButtonStyle = styled.section(
  ({
    variant,
    size,
    center,
    theme: {
      color: { borderGray, primary2, primary, white },
      utils: { flex }
    }
  }) =>
    t(css`
      ${center && flex("center", "center")}
      button {
        all: unset;
        outline: none;
        border-radius: 16px;
        background-color: white;
        height: 56px;
        border: 1px solid ${borderGray};
        padding: 0px 32px;
        font-size: 18px;
        font-weight: 600;
        cursor: pointer;
        ${flex("center", "center", "column")};
        ${size === "lg" &&
        `
          height: 50px;
        `}
        ${variant === "primary" &&
        `
          background: ${primary};
          color: ${white};
          border: none;
        `}
        ${variant === "secondary" &&
        `
          background: ${primary2};
          color: ##232A24;
          border: none;
        `}
        ${variant === "border" &&
        `
          background: transparent;
          color: ${white};
          border: 1px solid ${white};
        `}
      }
    `)
);
