import React from "react";

import { ButtonStyle } from "./form.style";

const Button = ({
  children,
  variant = "primary",
  size = "md",
  center,
  ...params
}) => {
  return (
    <ButtonStyle variant={variant} center={center} size={size}>
      <button type="button" className="button" {...params}>
        {children}
      </button>
    </ButtonStyle>
  );
};

export default Button;
