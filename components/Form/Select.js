import React from "react";
import { useField } from "formik";
import { Select } from "antd";

import StyleWrapper from "./form.style";

export default ({ formik, label, options, ...props }) => {
  const [field, meta] = useField(props);

  const { setFieldValue } = formik;
  const { error, touched } = meta;
  const { name } = props;

  return (
    <StyleWrapper className="input select-input">
      {label && <label htmlFor={name}>{label}</label>}

      <Select
        id={name}
        data-test-id={name}
        {...props}
        {...field}
        onChange={(option) => setFieldValue(name, option)}
      >
        {options.map(({ key, title }) => (
          <Select.Option key={key} title={title} value={key}>
            {title}
          </Select.Option>
        ))}
      </Select>

      {touched && error && (
        <div className="text-danger">
          <span className="text-danger--text">{error}</span>
        </div>
      )}
    </StyleWrapper>
  );
};
