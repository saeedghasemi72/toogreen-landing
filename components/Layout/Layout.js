import React from "react";

import { Header } from "components";

import StyleWrapper from "./layout.style";

export default ({ children }) => {
  return (
    <>
      <StyleWrapper className="container">
        <Header />
        {children}
        {/* <Footer /> */}
      </StyleWrapper>
    </>
  );
};
