import { css, createGlobalStyle } from "styled-components";
import { styleTranformHelper as t } from "../helpers";

export default createGlobalStyle(
  ({
    theme: {
      color: { bgRed, text4, text1, primary1, primary2, bg0 },
      utils: { flex },
      constant: { screens, fonts }
    }
  }) =>
    t(css`
      html {
        scroll-behavior: smooth;
        @media screen and (max-width: ${screens.sm}) {
          font-size: 14px;
        }
      }

      *,
      *:before,
      *:after {
        box-sizing: border-box !important;
      }

      body {
        font-family: "Montserrat", "cursive" !important;
        margin: 0;
        box-sizing: border-box;
        overflow-x: hidden;
        background-color: white;
      }
      h1,
      h2,
      h3,
      h4,
      h5,
      h6,
      input,
      textarea {
        font-family: "Montserrat", "cursive" !important;
      }

      .container {
        padding: 0;
        margin: 0 auto;
        @media screen and (max-width: ${screens.sm}) {
          width: 100%;
          padding-left: 16px !important;
          padding-right: 16px !important;
        }
        @media screen and (min-width: ${screens.sm}) {
          width: 540px;
        }
        @media screen and (min-width: ${screens.md}) {
          width: 720px;
        }
        @media screen and (min-width: ${screens.lg}) {
          width: 960px;
        }
        @media screen and (min-width: ${screens.xl}) {
          width: 1176px;
        }
        @media screen and (min-width: ${screens.xxl}) {
          width: 1400px;
        }
        /* @media screen and (min-width: ${screens.xxxl}) {
          width: 1600px;
        } */
        /* @media screen and (min-width: ${screens.xxxl}) {
          width: 1600px;
        } */
      }

      a {
        text-decoration: none;
        /* color: ${text1}; */
      }

      .ant-menu:not(.ant-menu-horizontal) .ant-menu-item-selected {
        background-color: transparent;
      }

      a.primary {
        font-size: 14px;
        height: 40px;
        background-color: ${primary1};
        border-radius: 4px;
        color: ${text4};
        padding: 0 16px;
        ${flex("center", "center")}
      }

      a.button-opacity {
        background-color: rgba(8, 35, 25, 0.5);
        border-radius: 4px;
        color: ${text4};
        text-decoration: none;
        font-size: 14px;
        height: 40px;
        padding: 0 16px;
        ${flex("center", "center")}
      }

      .image-16-9 {
        padding-top: 56.2%;
        position: relative;
        img {
          position: absolute;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
          object-fit: cover;
        }
      }
      .res-message {
        margin-top: 30px;
        color: ${bgRed};
      }
      .ltr {
        text-align: left;
      }
      .mt-1 {
        margin-top: 1rem;
      }
      .mt-2 {
        margin-top: 2rem;
      }
      .mt-3 {
        margin-top: 3rem;
      }
      .mt-4 {
        margin-top: 4rem;
      }
      .mt-5 {
        margin-top: 5rem;
      }
      .ltr {
        direction: ltr;
      }
      .ant-slider-track {
        background: ${primary2};
      }
      .ant-slider-handle {
        background: ${primary1};
        border: 1px solid ${primary1};
      }
      .imageWrapper {
        img {
          width: 100%;
        }
      }
      .text-danger {
        color: red;
      }
      .haeder-dropdown {
        min-width: 532px !important;
        max-width: 832px !important;
        top: 75px !important;
        /* right: 336px !important; */
        left: 0 !important;
        margin: 0;

        ul {
          /* padding: 5px ​16px; */
          padding: 0;
          max-width: 530px;
        }
        .ant-dropdown-menu-item {
          position: relative;
          &:after {
            content: "";
            position: absolute;
            width: 0;
            bottom: 0;
            right: 0;
            background-color: ${primary2};
            height: 2px;
            transition: all ease 400ms;
          }
        }
        .ant-dropdown-menu-item {
          &:hover {
            &:after {
              content: "";
              position: absolute;
              width: 100%;
            }
          }
        }
      }

      /* display: none */
      .sm-none {
        @media screen and (max-width: ${screens.sm}) {
          display: none !important;
        }
      }
      .md-none {
        @media screen and (max-width: ${screens.md}) {
          display: none !important;
        }
      }
      .lg-none {
        @media screen and (max-width: ${screens.lg}) {
          display: none !important;
        }
      }
      .xl-none {
        @media screen and (max-width: ${screens.xl}) {
          display: none !important;
        }
      }
      .xxl-none {
        @media screen and (max-width: ${screens.xxl}) {
          display: none !important;
        }
      }

      /* width 100% */
      .sm-100 {
        @media screen and (max-width: ${screens.sm}) {
          width: 100% !important;
        }
      }
      .md-100 {
        @media screen and (max-width: ${screens.md}) {
          width: 100% !important;
        }
      }
      .lg-100 {
        @media screen and (max-width: ${screens.lg}) {
          width: 100% !important;
        }
      }
      .xl-100 {
        @media screen and (max-width: ${screens.xl}) {
          width: 100% !important;
        }
      }
      .xxl-100 {
        @media screen and (max-width: ${screens.xxl}) {
          width: 100% !important;
        }
      }
    `)
);
