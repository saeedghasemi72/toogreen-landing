/* eslint-disable */

// Convert the px unit in the string style to rem
// Support multi-line matching
function r(pxValue) {
  const ratio = 16; // Set according to the project configuration ratio

  // for template literals
  if (Array.isArray(pxValue)) {
    pxValue = pxValue[0];
  }

  pxValue = parseInt(pxValue, 10);

  return `${pxValue / ratio}rem`;
}

// Implement the transformPxToRem beforehand to pass the style to styled
function transformPxToRem(style) {
  // Avoid dealing with situations such as functions
  if (typeof style !== "string") {
    return style;
  }

  return style.replace(/\d+px/gm, (matched) => {
    return r(matched);
  });
}

export default function styleTranform(styles) {
  // let styles = css(strings, ...interpolations); // css is a helper of styled-components
  const styleTransform = styles.map(transformPxToRem);

  // Simulate raw call
  return [[""], styleTransform];
}
