import { useTranslation } from "next-i18next";

import StyledWrapper from "./ourCategories.ctr.style";

const OurCategoriesCTR = () => {
  const { t } = useTranslation("home");
  const categories = [
    {
      image: "/images/burgers.png",
      name: t("our_categories.burgers"),
      resNumber: "32",
      foodNumber: "86"
    },
    {
      image: "/images/pizzas.png",
      name: t("our_categories.pizzas"),
      resNumber: "28",
      foodNumber: "112"
    },
    {
      image: "/images/soups.png",
      name: t("our_categories.soups"),
      resNumber: "41",
      foodNumber: "55"
    },
    {
      image: "/images/salads.png",
      name: t("our_categories.salads"),
      resNumber: "19",
      foodNumber: "28"
    },
    {
      image: "/images/appetizers.png",
      name: t("our_categories.appetizers"),
      resNumber: "20",
      foodNumber: "36"
    },
    {
      image: "/images/desserts.png",
      name: t("our_categories.desserts"),
      resNumber: "14",
      foodNumber: "38"
    }
  ];
  return (
    <div className="container">
      <StyledWrapper>
        <p className="dec">{t("our_categories.dec")}</p>
        <p className="title">{t("our_categories.title")}</p>
        <div className="categoryWrapper">
          {categories.map((cat) => (
            <div className="categoryItem" key={cat.name}>
              <img src={cat.image} alt={cat.name} />
              <p className="categoryItem__name">{cat.name}</p>
              <p className="categoryItem__count restaurants">
                <span>{cat.resNumber}</span>
                <span>{t("our_categories.restaurants")}</span>
              </p>
              <p className="categoryItem__count">
                <span>{cat.foodNumber}</span>
                <span>{t("our_categories.foods")}</span>
              </p>
            </div>
          ))}
        </div>
      </StyledWrapper>
    </div>
  );
};

export default OurCategoriesCTR;
