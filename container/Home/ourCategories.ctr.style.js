import styled, { css } from "styled-components";
import { styleTranformHelper as t } from "helpers";

export default styled.div(
  ({
    theme: {
      color: { black, grayLight, bgGreen },
      utils: { flex },
      constant: { screens }
    }
  }) =>
    t(css`
      padding-top: 100px;
      .dec {
        color: ${grayLight};
        text-align: center;
        font-size: 24px;
        margin: 0;
        line-height: 18px;
      }
      .title {
        font-size: 40px;
        text-align: center;
        color: ${black};
        font-weight: bold;
      }
      .categoryWrapper {
        ${flex("space-between", "center")};
        @media screen and (max-width: ${screens.xl}) {
          overflow: scroll;
        }
      }
      .categoryItem {
        background-color: ${bgGreen};
        border-radius: 24px;
        padding: 24px;
        @media screen and (max-width: ${screens.lg}) {
          margin: 0 16px;
        }
        @media screen and (max-width: ${screens.xxl}) {
          padding: 20px;
        }
        img {
          width: 143px;
          display: block;
          margin: auto;
        }
        &__name {
          font-size: 18px;
          font-weight: bold;
          text-align: center;
          margin-bottom: 32px;
        }
        &__count.restaurants {
          margin: 0;
        }
        &__count {
          text-align: center;
          span:first-child {
            font-weight: bold;
            font-size: 16px;
          }
          span:last-child {
            font-size: 16px;
            margin-left: 5px;
          }
        }
      }
    `)
);
