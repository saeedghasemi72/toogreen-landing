import { useTranslation } from "next-i18next";
import { Row, Col } from "antd";

import StyledWrapper from "./downloadApp.ctr.style";

const OurMissionCTR = () => {
  const { t } = useTranslation("home");
  return (
    <div className="container">
      <StyledWrapper>
        <Row
          gutter={[
            { xs: 0, md: 80 },
            { xs: 0, md: 0 }
          ]}
        >
          <Col md={24} xl={12} xxl={12}>
            <p className="topText">{t("download.topText")}</p>
            <p className="title">{t("download.title")}</p>
            <p className="dec">{t("download.dec")}</p>
            <div className="inputWrapper">
              <input type="text" placeholder={t("download.inputPlaceholder")} />
              <button type="button">Send App Link</button>
            </div>
            <p className="downloadText">{t("download.downloadText")}</p>
            <div className="downloadWrapper">
              <a href="/" className="downloadBox">
                <img src="/images/appstore.svg" alt="plate" />
                <div>
                  <p>{t("download.download_on_the")}</p>
                  <p>{t("download.app_store")}</p>
                </div>
              </a>
              <a href="/" className="downloadBox">
                <img src="/images/googleplay.svg" alt="plate" />
                <div>
                  <p>{t("download.download_on_the")}</p>
                  <p>{t("download.googlePlay")}</p>
                </div>
              </a>
            </div>
          </Col>
          <Col md={24} xl={12} xxl={12}>
            <div className="recipeImageWrapper xl-none">
              <img src="/images/download-app-xd.png" alt="download" />
            </div>
          </Col>
        </Row>
      </StyledWrapper>
    </div>
  );
};

export default OurMissionCTR;
