import styled, { css } from "styled-components";
import { styleTranformHelper as t } from "helpers";

export default styled.div(
  ({
    theme: {
      color: { grayLight, black, bgGreen },
      utils: { flex },
      constant: { screens }
    }
  }) =>
    t(css`
      padding-top: 100px;
      .dec {
        color: ${grayLight};
        text-align: center;
        font-size: 24px;
        margin: 0;
        line-height: 18px;
      }
      .title {
        font-size: 40px;
        text-align: center;
        color: ${black};
        font-weight: bold;
      }
      .box {
        ${flex("center", "center", "column")};

        .awesomeIconWrapper {
          text-align: center;
        }
        p {
          text-align: center;
        }
        p:first-of-type {
          font-size: 20px;
          font-weight: bold;
          margin-top: 24px;
        }
        p:last-of-type {
          font-size: 16px;
          color: ${grayLight};
        }
        @media screen and (max-width: ${screens.md}) {
          flex-direction: row;
          justify-content: flex-start;
          margin: 16px 0;
          p {
            text-align: left !important;
            padding-left: 16px;
          }
          p:first-of-type {
            margin: 0;
          }
        }
      }
    `)
);
