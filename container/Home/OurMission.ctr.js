import { useTranslation } from "next-i18next";
import { Row, Col } from "antd";

import StyledWrapper from "./ourMission.ctr.style";

const OurMissionCTR = () => {
  const { t } = useTranslation("home");
  return (
    <div className="container">
      <StyledWrapper>
        <p className="dec">{t("our_mission.dec")}</p>
        <p className="title">{t("our_mission.title")}</p>
        <Row
          gutter={[
            { xs: 0, md: 64 },
            { xs: 0, md: 64 }
          ]}
        >
          <Col md={12}>
            <div className="bigImage md-none">
              <img src="/images/ourMission.svg" alt="" />
            </div>
          </Col>
          <Col md={12}>
            <div className="missonItem">
              <div className="missonItem__image">
                <img src="/images/missionHelping.svg" alt="misson" />
              </div>
              <div>
                <p className="missonItem__title">
                  {t("our_mission.helpingTitle")}
                </p>
                <p className="missonItem__dec">{t("our_mission.helpingDec")}</p>
              </div>
            </div>

            <div className="missonItem">
              <div className="missonItem__image">
                <img src="/images/missionVarious.svg" alt="misson" />
              </div>
              <div>
                <p className="missonItem__title">
                  {t("our_mission.variousTitle")}
                </p>
                <p className="missonItem__dec">{t("our_mission.variousDec")}</p>
              </div>
            </div>

            <div className="missonItem">
              <div className="missonItem__image">
                <img src="/images/missionfast.svg" alt="misson" />
              </div>
              <div>
                <p className="missonItem__title">
                  {t("our_mission.fastTitle")}
                </p>
                <p className="missonItem__dec">{t("our_mission.fastDec")}</p>
              </div>
            </div>
          </Col>
        </Row>
      </StyledWrapper>
    </div>
  );
};

export default OurMissionCTR;
