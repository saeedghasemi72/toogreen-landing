import { useTranslation } from "next-i18next";
import { Row, Col } from "antd";

import StyledWrapper from "./awesomeService.ctr.style";

const OurMissionCTR = () => {
  const { t } = useTranslation("home");
  return (
    <div className="container">
      <StyledWrapper>
        <p className="dec">{t("awesome_service.dec")}</p>
        <p className="title">{t("awesome_service.title")}</p>
        <Row
          gutter={[
            { xs: 0, md: 64 },
            { xs: 0, md: 64 }
          ]}
        >
          <Col xs={24} md={8}>
            <div className="box">
              <div className="awesomeIconWrapper">
                <img src="/images/awesome-quality.svg" alt="awesome" />
              </div>
              <div>
                <p>{t("awesome_service.qualityFoodTitle")}</p>
                <p>{t("awesome_service.qualityFoodDec")}</p>
              </div>
            </div>
          </Col>
          <Col xs={24} md={8}>
            <div className="box">
              <div className="awesomeIconWrapper">
                <img src="/images/awesome-super.svg" alt="awesome" />
              </div>
              <div>
                <p>{t("awesome_service.superTasteTitle")}</p>
                <p>{t("awesome_service.superTasteDec")}</p>
              </div>
            </div>
          </Col>
          <Col xs={24} md={8}>
            <div className="box">
              <div className="awesomeIconWrapper">
                <img src="/images/awesome-fast.svg" alt="awesome" />
              </div>
              <div>
                <p>{t("awesome_service.fastDeliveryTitle")}</p>
                <p>{t("awesome_service.fastDeliveryDec")}</p>
              </div>
            </div>
          </Col>
        </Row>
      </StyledWrapper>
    </div>
  );
};

export default OurMissionCTR;
