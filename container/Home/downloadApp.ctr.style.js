import styled, { css } from "styled-components";
import { styleTranformHelper as t } from "helpers";

export default styled.div(
  ({
    theme: {
      color: { black, bgGreen, bgWhite, primary, white, grayLight },
      utils: { flex },
      constant: { screens }
    }
  }) =>
    t(css`
      background-color: ${bgGreen};
      border-radius: 32px;
      padding: 64px;
      /* padding-bottom: 0 !important; */
      @media screen and (max-width: ${screens.md}) {
        padding: 16px !important;
      }
      @media screen and (min-width: ${screens.xl}) {
        padding-bottom: 0 !important;
      }
      .recipeImageWrapper {
        
        ${flex("center", "center")};
        img {
          /* width: 100%; */
          /* height: 640px; */
          /* @media screen and (min-width: ${screens.md}) {
            height: 660px !important;
          }
          @media screen and (min-width: ${screens.xxl}) {
            height: 620px !important;
          } */
        }
      }
      .topText {
        font-size: 32px;
        color: ${primary};
        margin-bottom: 5px;
      }
      .title {
        font-size: 56px;
        font-weight: bold;
        margin: 0;
        line-height: 64px;
      }
      .dec {
        font-size: 16px;
        color: ${black};
        margin: 24px 0;
      }
      .downloadWrapper {
        ${flex("flex-start", "center")};
        .downloadBox {
          padding: 12px 24px;
          ${flex("center", "center")};
          background-color: ${bgWhite};
          border-radius: 16px;
          img {
            margin-right: 11px;
          }
          div {
            ${flex("center", "flex-start", "column")};

            p {
              margin: 0;
            }
            p:last-child {
              font-weight: bold;
              font-size: 16px;
            }
          }
          @media screen and (max-width: ${screens.md}) {
            padding: 12px 16px;

            p:last-child {
              font-size: 15px;
            }
          }
          }
        }
        .downloadBox:last-child {
          margin-left: 24px;
          @media screen and (max-width: ${screens.md}) {
            margin-left: 16px;
          }
        }
        @media screen and (max-width: ${screens.md}) {
          padding: 12px 0;
          margin-bottom: 32px;
        }
      }
      .inputWrapper {
        position: relative;
        margin-bottom: 64px;
        input {
          height: 72px;
          background-color: ${bgWhite};
          width: 100%;
          border-radius: 16px;
          border: none;
          outline: none;
          padding: 16px 32px;
        }
        button {
          all: unset;
          position: absolute;
          right: 8px;
          top: 0;
          bottom: 0;
          height: 56px;
          margin: auto;
          border-radius: 16px;
          padding: 16px 32px;
          background-color: ${primary};
          color: ${white};
          /* font-weight: bold; */
          font-size: 18px;

          @media screen and (max-width: ${screens.md}) {
            position: unset;
            width: 100%;
            text-align: center;
            height: 56px;
            margin-top: 16px;
          }
        }
      }
      .downloadText {
        font-size: 16px;
        color: ${grayLight};
      }
    `)
);
