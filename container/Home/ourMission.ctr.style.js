import styled, { css } from "styled-components";
import { styleTranformHelper as t } from "helpers";

export default styled.div(
  ({
    theme: {
      color: { grayLight, black, bgGreen },
      utils: { flex }
    }
  }) =>
    t(css`
      padding: 100px 0;
      .dec {
        color: ${grayLight};
        text-align: center;
        font-size: 24px;
        margin: 0;
        line-height: 18px;
      }
      .title {
        font-size: 40px;
        text-align: center;
        color: ${black};
        font-weight: bold;
      }
      .bigImage {
        text-align: center;
        height: 100%;
        ${flex("center", "flex-end")}
        img {
          max-width: 472x;
        }
      }
      .missonItem {
        ${flex("center", "center")};

        &__image {
          ${flex("center", "center")};
          min-width: 80px;
          min-height: 80px;
          border-radius: 500px;
          background-color: ${bgGreen};
          margin-right: 24px;
        }
        &__title {
          font-size: 20px;
          font-weight: bold;
          margin-bottom: 5px;
        }
        &__dec {
          font-size: 16px;
          margin: 0;
          color: ${grayLight};
        }
      }
      .missonItem:first-child {
        padding-top: 32px;
      }
      .missonItem:last-child {
        padding-bottom: 32px;
      }
      .missonItem:not(:last-child) {
        margin-bottom: 75px;
      }
    `)
);
