export { default as AwesomeServicesCTR } from "./AwesomeServices.ctr";
export { default as DownloadAppCTR } from "./DownloadApp.ctr";
export { default as OurCategoriesCTR } from "./OurCategories.ctr";
export { default as OurMissionCTR } from "./OurMission.ctr";
export { default as OurRecipeCTR } from "./OurRecipe.ctr";
