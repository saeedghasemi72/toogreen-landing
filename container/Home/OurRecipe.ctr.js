import { useTranslation } from "next-i18next";
import { Row, Col } from "antd";

import StyledWrapper from "./ourRecipe.ctr.style";

const OurMissionCTR = () => {
  const { t } = useTranslation("home");
  return (
    <StyledWrapper>
      <div className="container">
        <Row
          gutter={[
            { xs: 0, md: 32, xl: 64 },
            { xs: 0, md: 0, xl: 0 }
          ]}
        >
          <Col md={12}>
            <p className="topText">{t("our_recipe.topText")}</p>
            <p className="title">{t("our_recipe.title")}</p>
            <p className="dec">{t("our_recipe.dec")}</p>
            <img
              src="/images/ourRecipe-phone.png"
              alt="recipe"
              className="md-100"
            />
          </Col>
          <Col md={12}>
            <div className="recipeImageWrapper md-none">
              <img src="/images/our-recipe.png" alt="recipe" />
            </div>
          </Col>
        </Row>
      </div>
    </StyledWrapper>
  );
};

export default OurMissionCTR;
