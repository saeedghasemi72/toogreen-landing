import styled, { css } from "styled-components";
import { styleTranformHelper as t } from "helpers";

export default styled.div(
  ({
    theme: {
      color: { black, secondary, bgLightOrange },
      utils: { flex }
    }
  }) =>
    t(css`
      /* padding:  */
      background-color: ${bgLightOrange};
      .recipeImageWrapper {
        padding: 64px 16px;
        height: 100%;
        ${flex("center", "center")};
        img {
          width: 100%;
        }
      }
      .topText {
        font-size: 32px;
        color: ${secondary};
        margin-top: 64px;
        margin-bottom: 5px;
      }
      .title {
        font-size: 56px;
        font-weight: bold;
        margin: 0;
        line-height: 64px;
      }
      .dec {
        font-size: 16px;
        color: ${black};
        margin: 24px 0;
        font-family: "Baloo Thambi 2";
      }
    `)
);
