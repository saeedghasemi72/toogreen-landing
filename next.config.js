// const path = require("path");

// module.exports = {
//   sassOptions: {
//     includePaths: [path.join(__dirname, "styles")]
//   }
// };

const withSass = require("@zeit/next-sass");
const withLess = require("@zeit/next-less");
const withCSS = require("@zeit/next-css");
const { i18n } = require("./next-i18next.config");

const isProd = process.env.NODE_ENV === "production";

// fix: prevents error when .less files are required by node
// if (typeof require !== "undefined") {
//   require.extensions[".less"] = (file) => {};
// }

module.exports = withCSS({
  cssModules: true,
  cssLoaderOptions: {
    importLoaders: 1,
    localIdentName: "[local]___[hash:base64:5]"
  },
  ...withLess(
    withSass({
      i18n,
      lessLoaderOptions: {
        javascriptEnabled: true
      }
    })
  )
});
