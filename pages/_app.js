import { ConfigProvider } from "antd";
import { appWithTranslation } from "next-i18next";
import path from "path";
import nextI18NextConfig from "../next-i18next.config.js";

import GlobalStyle from "../styles/global.style";
import Theme from "../theme";

import "../antd.less";

const App = ({ Component, pageProps }) => {
  return (
    <>
      <title>TooGreen</title>
      <Theme>
        <GlobalStyle />
        <ConfigProvider>
          <Component {...pageProps} />
        </ConfigProvider>
      </Theme>
    </>
  );
};

export default appWithTranslation(App, nextI18NextConfig);
