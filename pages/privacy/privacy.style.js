import styled, { css } from "styled-components";
import { styleTranformHelper as t } from "helpers";

export default styled.div(
  ({
    theme: {
      color: { black, primary, grayLight },
      constant: { screens }
    }
  }) =>
    t(css`
      .privacyWrapper {
        padding: 100px 140px 0;
        max-width: 992px;
        margin: auto;
        @media screen and (max-width: ${screens.sm}) {
          padding: 100px 0 0;
        }
        color: ${grayLight};
        font-family: "Baloo Thambi 2";
        font-size: 16px;
        ul {
          list-style: none;
          li {
            position: relative;
            &:before {
              content: "";
              position: absolute;
              width: 6px;
              height: 6px;
              background-color: ${primary};
              border-radius: 100px;
              left: -24px;
              top: 10px;
            }
          }
        }
        .title {
          font-weight: bold;
          font-size: 26px;
          margin-top: 24px;
          margin-bottom: 12px;
          color: ${black};
          font-family: "Montserrat";
        }
        .update {
          p {
            span:last-child {
              color: ${black};
              margin-left: 5px;
            }
          }
        }
        .download {
          font-weight: bold;
          font-size: 16px;
          color: ${black};
          text-decoration: underline;
        }
        .email {
          font-weight: bold;
          font-size: 16px;
          color: ${black};
        }
      }
    `)
);
