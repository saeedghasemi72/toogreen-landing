import { Header, Footer } from "components";

import StyledWrapper from "./privacy.style";

const Privacy = () => {
  return (
    <StyledWrapper>
      <Header
        title={
          <p>
            Toogreen <br />
            Privacy Notice
          </p>
        }
      />
      <div className="container">
        <div className="privacyWrapper">
          <p className="title">Introduction</p>
          <p>
            When you use Toogreen, you trust us with your personal data. We’re
            committed to keeping that trust. That starts with helping you
            understand our privacy practices. This notice describes the personal
            data we collect, how it’s used and shared, and your choices
            regarding this data. We recommend that you read this al, which
            highlights key points about our privacy practices.
          </p>
          <div className="update">
            <p>
              <span>Last modified:</span>
              <span>October 15, 2020</span>
            </p>
            <p>
              <span>Effective date:</span>
              <span>October 15, 2020</span>
            </p>
            <a href="#" className="download">
              Download previous version
            </a>
          </div>
          <p className="title">Overview</p>
          <p>
            Toogreen Private Limited(Formerly known as Toogreen Media Private
            Limited) and/or its affiliates (&quot;Toogreen,&quot; the
            &quot;Company,&quot; &quot;we,&quot; &quot;us,&quot; and
            &quot;our,&quot;) respect your privacy and is committed to
            protecting it through its compliance with its privacy policies. This
            policy describes:
          </p>
          <ul>
            <li>
              the types of information that Toogreen may collect from you when
              you access or use its websites, applications and other online
              services (collectively, referred as &quot;Services&quot;); and
            </li>
            <li>
              its practices for collecting, using, maintaining, protecting and
              disclosing that information.
            </li>
          </ul>
          <p>
            This policy applies only to the information Toogreen collects
            through its Services, in email, text and other electronic
            communications sent through or in connection with its Services.
          </p>
          <p>
            This policy DOES NOT apply to information that you provide to, or
            that is collected by, any third-party, such as restaurants at which
            you make reservations and/or pay through Toogreen&apos;s Services
            and social networks that you use in connection with its Services.
            Toogreen encourages you to consult directly with such third-parties
            about their privacy practices.
          </p>
          <p>
            Please read this policy carefully to understand Toogreen&apos;s
            policies and practices regarding your information and how Toogreen
            will treat it. By accessing or using its Services and/or registering
            for an account with Toogreen, you agree to this privacy policy and
            you are consenting to Toogreen&apos;s collection, use, disclosure,
            retention, and protection of your personal information as described
            here. If you do not provide the information Toogreen requires,
            Toogreen may not be able to provide all of its Services to you.
          </p>
          <p>
            If you reside in a country within the European Union/European
            Economic Area (EAA), Toogreen Media Portugal, Unipessoal LDA ,
            located at Avenida 24 de Julho, N 102-E, 1200-870, Lisboa, Portugal,
            will be the controller of your personal data provided to, or
            collected by or for, or processed in connection with our Services;
            If you reside in United States of America, Toogreen USA LLC, located
            at 7427 Matthews Mint Hill Rd., STE 105, #324, Mint Hill, NC 28227
            will be the controller of your personal data provided to, or
            collected by or for, or processed in connection with our Services;
          </p>
          <p>
            If you reside in any other part of the world, Toogreen Private
            Limited, located at Ground Floor, Tower C, Vipul Tech Square, Sector
            43, Golf Course Road, Gurugram - 122009, Haryana, India will be the
            controller of your personal data provided to, or collected by or
            for, or processed in connection with our Services.
          </p>
          <p>
            Your data controller is responsible for the collection, use,
            disclosure, retention, and protection of your personal information
            in accordance with its privacy standards as well as any applicable
            national laws. Your data controller may transfer data to other
            members of Toogreen as described in this Privacy Policy. Toogreen
            may process and retain your personal information on its servers in
            India where its data centers are located, and/or on the servers of
            its third parties (in or outside India), having contractual
            relationships with Toogreen.
          </p>
          <p>
            This policy may change from time to time, your continued use of
            Toogreen&apos;s Services after it makes any change is deemed to be
            acceptance of those changes, so please check the policy periodically
            for updates.
          </p>
          <p className="title">Contact us</p>
          <p>
            If you have any queries relating to the processing/ usage of
            information provided by you or Toogreen&apos;s Privacy Policy, you
            may email the Data Protection Officer (DPO) at{" "}
            <span className="email">privacy@toogreen.com</span> or write to us
            at the following address.
          </p>
        </div>
      </div>
      <Footer />
    </StyledWrapper>
  );
};

export default Privacy;
