import { ErrorBoundary } from "components";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Privacy from "./Privacy";

export default () => (
  <ErrorBoundary>
    <Privacy />
  </ErrorBoundary>
);

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ["common", "header", "footer"]))
  }
});
