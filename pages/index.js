import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import { HomeHeader, Footer } from "components";
import {
  OurCategoriesCTR,
  OurMissionCTR,
  OurRecipeCTR,
  AwesomeServicesCTR,
  DownloadAppCTR
} from "container";

import StyledWrapper from "./index.style";

const Home = ({ locale }) => {
  return (
    <StyledWrapper>
      <HomeHeader locale={locale} />
      <OurCategoriesCTR />
      <OurMissionCTR />
      <OurRecipeCTR />
      <AwesomeServicesCTR />
      <DownloadAppCTR />
      <Footer />
    </StyledWrapper>
  );
};

export const getStaticProps = async ({ locale }) => {
  return {
    props: {
      locale,
      ...(await serverSideTranslations(locale, [
        "common",
        "header",
        "home",
        "footer"
      ]))
    }
  };
};

export default Home;
