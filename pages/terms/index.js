import { ErrorBoundary } from "components";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Terms from "./Terms";

export default () => (
  <ErrorBoundary>
    <Terms />
  </ErrorBoundary>
);

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ["common", "header", "footer"]))
  }
});
