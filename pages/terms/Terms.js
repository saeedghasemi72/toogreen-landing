import { Header, Footer } from "components";

import StyledWrapper from "./terms.style";

const Terms = () => {
  return (
    <StyledWrapper>
      <Header title={<p>Terms & Conditions</p>} />
      <div className="container">
        <div className="termsWrapper">
          <p className="title">Acceptance of terms</p>
          <p>
            Thank you for using Toogreen. These Terms of Service (the
            &quot;Terms&quot;) are intended to make you aware of your legal
            rights and responsibilities with respect to your access to and use
            of the Toogreen website at www.Toogreen.com (the &quot;Site&quot;)
            and any related mobile or software applications (&quot;Toogreen
            Platform&quot;) including but not limited to delivery of information
            via the website whether existing now or in the future that link to
            the Terms (collectively, the &quot;Services&quot;).
          </p>
          <p>
            These Terms are effective for all existing and future Toogreen
            users, including but without limitation to users having access to
            &apos;restaurant business page&apos; to manage their claimed
            business listings.
          </p>
          <p>
            Please read these Terms carefully. By accessing or using the
            Toogreen Platform, you are agreeing to these Terms and concluding a
            legally binding contract with Toogreen Private Limited(Formerly
            known as Toogreen Media Private Limited) and/or its affiliates or,
            if you are in the United States, with Toogreen USA, LLC and/or its
            affiliates (hereinafter collectively referred to as
            &quot;Toogreen&quot;). You may not use the Services if you do not
            accept the Terms or are unable to be bound by the Terms. Your use of
            the Toogreen Platform is at your own risk, including the risk that
            you might be exposed to content that is objectionable, or otherwise
            inappropriate.
          </p>
          <p>
            In order to use the Services, you must first agree to the Terms. You
            can accept the Terms by:
          </p>
          <ul>
            <li>
              Clicking to accept or agree to the Terms, where it is made
              available to you by Toogreen in the user interface for any
              particular Service; or
            </li>
            <li>
              Actually using the Services. In this case, you understand and
              agree that Toogreen will treat your use of the Services as
              acceptance of the Terms from that point onwards.
            </li>
          </ul>

          <p className="title">Definitions</p>
          <p className="subTitle">- User</p>
          <p>
            &quot;User&quot; or &quot;You&quot; or &quot;Your&quot; refers to
            you, as a user of the Services. A user is someone who accesses or
            uses the Services for the purpose of sharing, displaying, hosting,
            publishing, transacting, or uploading information or views or
            pictures and includes other persons jointly participating in using
            the Services including without limitation a user having access to
            &apos;restaurant business page&apos; to manage claimed business
            listings or otherwise.
          </p>
          <p className="subTitle">- Content</p>
          <p>
            &quot;Content&quot; will include (but is not limited to) reviews,
            images, photos, audio, video, location data, nearby places, and all
            other forms of information or data. &quot;Your content&quot; or
            &quot;User Content&quot; means content that you upload, share or
            transmit to, through or in connection with the Services, such as
            likes, ratings, reviews, images, photos, messages, profile
            information, and any other materials that you publicly display or
            displayed in your account profile. &quot;Toogreen Content&quot;
            means content that Toogreen creates and make available in connection
            with the Services including, but not limited to, visual interfaces,
            interactive features, graphics, design, compilation, computer code,
            products, software, aggregate ratings, reports and other
            usage-related data in connection with activities associated with
            your account and all other elements and components of the Services
            excluding Your Content and Third Party Content. &quot;Third Party
            Content&quot; means content that comes from parties other than
            Toogreen or its users and is available on the Services.
          </p>
          <p className="subTitle">- Restaurant(s)</p>
          <p>
            &quot;Restaurant&quot; means the restaurants listed on Toogreen and
            any related mobile or software applications of Zomato.
          </p>
          <p className="title">Eligibility to use the services</p>
          <p>
            1. You hereby represent and warrant that you are at least eighteen
            (18) years of age or above and are fully able and competent to
            understand and agree the terms, conditions, obligations,
            affirmations, representations, and warranties set forth in these
            Terms.
          </p>
          <p>
            2. Compliance with Laws. You are in compliance with all laws and
            regulations in the country in which you live when you access and use
            the Services. You agree to use the Services only in compliance with
            these Terms and applicable law, and in a manner that does not
            violate our legal rights or those of any third party(ies).
          </p>
          <p className="title">Changes to the terms</p>
          <p>
            Toogreen may vary or amend or change or update these Terms, from
            time to time entirely at its own discretion. You shall be
            responsible for checking these Terms from time to time and ensure
            continued compliance with these Terms. Your use of Toogreen Platform
            after any such amendment or change in the Terms shall be deemed as
            your express acceptance to such amended/changed terms and you also
            agree to be bound by such changed/amended Terms.
          </p>
        </div>
      </div>
      <Footer />
    </StyledWrapper>
  );
};

export default Terms;
