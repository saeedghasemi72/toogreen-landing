import { Button } from "components";

import StyledWrapper from "./404.style";

export default function Custom404() {
  return (
    <StyledWrapper>
      <div className="content">
        <p>404</p>
        <h1>صفحه مورد نظر یافت نشد</h1>
        <Button variant="secondary">بازگشت به صفحه اصلی</Button>
      </div>
    </StyledWrapper>
  );
}
