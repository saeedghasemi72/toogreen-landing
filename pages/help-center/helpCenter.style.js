import styled, { css } from "styled-components";
import { styleTranformHelper as t } from "helpers";

export default styled.div(
  ({
    theme: {
      color: { primary, grayLight },
      constant: { screens }
    }
  }) =>
    t(css`
      .collapseWrapper {
        max-width: 992px;
        padding: 100px;
        margin: auto;
        @media screen and (max-width: ${screens.sm}) {
          padding: 48px 0;
        }
        .ant-collapse-header {
          font-size: 20px;
          font-weight: bold;
          .ant-collapse-arrow {
            transition: all ease 300ms;
            padding: 0;
            top: 20px;
          }
          .ant-collapse-arrow.active {
            transform: rotate(180deg);
            padding: 0;
            top: 20px;
            padding-top: 0;
            padding-bottom: 12px 16px;
            transition: all ease 300ms;
            color: ${primary};
          }
        }
        .ant-collapse-content-box {
          font-family: "Baloo Thambi 2";
          color: ${grayLight};
          font-size: 16px;
          p {
            padding-left: 16px;
          }
          ul {
            list-style: none;
            li {
              position: relative;
              &:before {
                content: "";
                position: absolute;
                width: 6px;
                height: 6px;
                background-color: ${primary};
                border-radius: 100px;
                left: -24px;
                top: 10px;
              }
            }
          }
        }
      }
    `)
);
