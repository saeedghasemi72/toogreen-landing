import { Header, Footer } from "components";
import { Collapse } from "antd";
import { CaretDownOutlined } from "@ant-design/icons";

import StyledWrapper from "./helpCenter.style";

const { Panel } = Collapse;

const helpCenter = () => {
  return (
    <StyledWrapper>
      <Header
        title={
          <p>
            Hi! How can we <br /> help you?
          </p>
        }
      />
      <div className="container">
        <div className="collapseWrapper">
          <Collapse
            defaultActiveKey={["1"]}
            ghost
            expandIcon={({ isActive }) => {
              return (
                <CaretDownOutlined
                  style={{ fontSize: "16px" }}
                  className={`${isActive ? "active" : ""}`}
                />
              );
            }}
          >
            <Panel
              header="The information we collect and how we use it"
              key="1"
            >
              <p>
                Toogreen Media Private Limited (&quot;Toogreen,&quot; the
                &quot;Company,&quot; &quot;we,&quot; &quot;us,&quot; and
                &quot;our”) collects several types of information from and about
                users of our Services, including:
              </p>
              <ul>
                <li>
                  Your Personal Information(&quot;PI&quot;) - Personal
                  Information is the information that can be associated with a
                  specific person and could be used to identify that specific
                  person whether from that data, or from the data and other
                  information that we have, or is likely to have access to. We
                  do not consider personal information to include information
                  that has been made anonymous or aggregated so that it can no
                  longer be used to identify a specific person, whether in
                  combination with other information or otherwise.
                </li>
                <li>
                  Information about your internet connection, the equipment you
                  use to access our Services and your usage details.
                </li>
              </ul>
              <p>We collect this information:</p>
              <ul>
                <li>directly from you when you provide it to us; and/or</li>
                <li>
                  automatically as you navigate through our Services
                  (information collected automatically may include usage
                  details, IP addresses and information collected through
                  cookies, web beacons and other tracking technologies).
                </li>
              </ul>
            </Panel>
            <Panel header="Information You Provide to Us" key="2">
              <p>
                Toogreen Media Private Limited (&quot;Toogreen,&quot; the
                &quot;Company,&quot; &quot;we,&quot; &quot;us,&quot; and
                &quot;our”) collects several types of information from and about
                users of our Services, including:
              </p>
              <ul>
                <li>
                  Your Personal Information(&quot;PI&quot;) - Personal
                  Information is the information that can be associated with a
                  specific person and could be used to identify that specific
                  person whether from that data, or from the data and other
                  information that we have, or is likely to have access to. We
                  do not consider personal information to include information
                  that has been made anonymous or aggregated so that it can no
                  longer be used to identify a specific person, whether in
                  combination with other information or otherwise.
                </li>
                <li>
                  Information about your internet connection, the equipment you
                  use to access our Services and your usage details.
                </li>
              </ul>
              <p>We collect this information:</p>
              <ul>
                <li>directly from you when you provide it to us; and/or</li>
                <li>
                  automatically as you navigate through our Services
                  (information collected automatically may include usage
                  details, IP addresses and information collected through
                  cookies, web beacons and other tracking technologies).
                </li>
              </ul>
            </Panel>
            <Panel header="Information about Your Friends" key="3">
              <p>
                Toogreen Media Private Limited (&quot;Toogreen,&quot; the
                &quot;Company,&quot; &quot;we,&quot; &quot;us,&quot; and
                &quot;our”) collects several types of information from and about
                users of our Services, including:
              </p>
              <ul>
                <li>
                  Your Personal Information(&quot;PI&quot;) - Personal
                  Information is the information that can be associated with a
                  specific person and could be used to identify that specific
                  person whether from that data, or from the data and other
                  information that we have, or is likely to have access to. We
                  do not consider personal information to include information
                  that has been made anonymous or aggregated so that it can no
                  longer be used to identify a specific person, whether in
                  combination with other information or otherwise.
                </li>
                <li>
                  Information about your internet connection, the equipment you
                  use to access our Services and your usage details.
                </li>
              </ul>
              <p>We collect this information:</p>
              <ul>
                <li>directly from you when you provide it to us; and/or</li>
                <li>
                  automatically as you navigate through our Services
                  (information collected automatically may include usage
                  details, IP addresses and information collected through
                  cookies, web beacons and other tracking technologies).
                </li>
              </ul>
            </Panel>
            <Panel header="Information about Your Messages" key="4">
              <p>
                Toogreen Media Private Limited (&quot;Toogreen,&quot; the
                &quot;Company,&quot; &quot;we,&quot; &quot;us,&quot; and
                &quot;our”) collects several types of information from and about
                users of our Services, including:
              </p>
              <ul>
                <li>
                  Your Personal Information(&quot;PI&quot;) - Personal
                  Information is the information that can be associated with a
                  specific person and could be used to identify that specific
                  person whether from that data, or from the data and other
                  information that we have, or is likely to have access to. We
                  do not consider personal information to include information
                  that has been made anonymous or aggregated so that it can no
                  longer be used to identify a specific person, whether in
                  combination with other information or otherwise.
                </li>
                <li>
                  Information about your internet connection, the equipment you
                  use to access our Services and your usage details.
                </li>
              </ul>
              <p>We collect this information:</p>
              <ul>
                <li>directly from you when you provide it to us; and/or</li>
                <li>
                  automatically as you navigate through our Services
                  (information collected automatically may include usage
                  details, IP addresses and information collected through
                  cookies, web beacons and other tracking technologies).
                </li>
              </ul>
            </Panel>
            <Panel
              header="Information We Collect Through Automatic Data Collection Technologies"
              key="5"
            >
              <p>
                Toogreen Media Private Limited (&quot;Toogreen,&quot; the
                &quot;Company,&quot; &quot;we,&quot; &quot;us,&quot; and
                &quot;our”) collects several types of information from and about
                users of our Services, including:
              </p>
              <ul>
                <li>
                  Your Personal Information(&quot;PI&quot;) - Personal
                  Information is the information that can be associated with a
                  specific person and could be used to identify that specific
                  person whether from that data, or from the data and other
                  information that we have, or is likely to have access to. We
                  do not consider personal information to include information
                  that has been made anonymous or aggregated so that it can no
                  longer be used to identify a specific person, whether in
                  combination with other information or otherwise.
                </li>
                <li>
                  Information about your internet connection, the equipment you
                  use to access our Services and your usage details.
                </li>
              </ul>
              <p>We collect this information:</p>
              <ul>
                <li>directly from you when you provide it to us; and/or</li>
                <li>
                  automatically as you navigate through our Services
                  (information collected automatically may include usage
                  details, IP addresses and information collected through
                  cookies, web beacons and other tracking technologies).
                </li>
              </ul>
            </Panel>
            <Panel
              header="Precise Location Information and How to Opt Out"
              key="5"
            >
              <p>
                Toogreen Media Private Limited (&quot;Toogreen,&quot; the
                &quot;Company,&quot; &quot;we,&quot; &quot;us,&quot; and
                &quot;our”) collects several types of information from and about
                users of our Services, including:
              </p>
              <ul>
                <li>
                  Your Personal Information(&quot;PI&quot;) - Personal
                  Information is the information that can be associated with a
                  specific person and could be used to identify that specific
                  person whether from that data, or from the data and other
                  information that we have, or is likely to have access to. We
                  do not consider personal information to include information
                  that has been made anonymous or aggregated so that it can no
                  longer be used to identify a specific person, whether in
                  combination with other information or otherwise.
                </li>
                <li>
                  Information about your internet connection, the equipment you
                  use to access our Services and your usage details.
                </li>
              </ul>
              <p>We collect this information:</p>
              <ul>
                <li>directly from you when you provide it to us; and/or</li>
                <li>
                  automatically as you navigate through our Services
                  (information collected automatically may include usage
                  details, IP addresses and information collected through
                  cookies, web beacons and other tracking technologies).
                </li>
              </ul>
            </Panel>
            <Panel header="Cookies and Other Electronic Tools" key="6">
              <p>
                Toogreen Media Private Limited (&quot;Toogreen,&quot; the
                &quot;Company,&quot; &quot;we,&quot; &quot;us,&quot; and
                &quot;our”) collects several types of information from and about
                users of our Services, including:
              </p>
              <ul>
                <li>
                  Your Personal Information(&quot;PI&quot;) - Personal
                  Information is the information that can be associated with a
                  specific person and could be used to identify that specific
                  person whether from that data, or from the data and other
                  information that we have, or is likely to have access to. We
                  do not consider personal information to include information
                  that has been made anonymous or aggregated so that it can no
                  longer be used to identify a specific person, whether in
                  combination with other information or otherwise.
                </li>
                <li>
                  Information about your internet connection, the equipment you
                  use to access our Services and your usage details.
                </li>
              </ul>
              <p>We collect this information:</p>
              <ul>
                <li>directly from you when you provide it to us; and/or</li>
                <li>
                  automatically as you navigate through our Services
                  (information collected automatically may include usage
                  details, IP addresses and information collected through
                  cookies, web beacons and other tracking technologies).
                </li>
              </ul>
            </Panel>
            <Panel
              header="Choices about how we use and disclose your information"
              key="7"
            >
              <p>
                Toogreen Media Private Limited (&quot;Toogreen,&quot; the
                &quot;Company,&quot; &quot;we,&quot; &quot;us,&quot; and
                &quot;our”) collects several types of information from and about
                users of our Services, including:
              </p>
              <ul>
                <li>
                  Your Personal Information(&quot;PI&quot;) - Personal
                  Information is the information that can be associated with a
                  specific person and could be used to identify that specific
                  person whether from that data, or from the data and other
                  information that we have, or is likely to have access to. We
                  do not consider personal information to include information
                  that has been made anonymous or aggregated so that it can no
                  longer be used to identify a specific person, whether in
                  combination with other information or otherwise.
                </li>
                <li>
                  Information about your internet connection, the equipment you
                  use to access our Services and your usage details.
                </li>
              </ul>
              <p>We collect this information:</p>
              <ul>
                <li>directly from you when you provide it to us; and/or</li>
                <li>
                  automatically as you navigate through our Services
                  (information collected automatically may include usage
                  details, IP addresses and information collected through
                  cookies, web beacons and other tracking technologies).
                </li>
              </ul>
            </Panel>
          </Collapse>
        </div>
      </div>
      <Footer />
    </StyledWrapper>
  );
};

export default helpCenter;
