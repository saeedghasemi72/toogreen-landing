import { ErrorBoundary } from "components";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import HelpCenter from "./helpCenter";

export default () => (
  <ErrorBoundary>
    <HelpCenter />
  </ErrorBoundary>
);

export const getStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ["common", "header", "footer"]))
  }
});
