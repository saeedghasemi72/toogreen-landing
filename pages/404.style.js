import styled, { css } from "styled-components";
import { styleTranformHelper as t } from "helpers";

export default styled.div(
  ({
    theme: {
      color: { primary2, text4, text1 },
      utils: { flex }
    }
  }) =>
    t(css`
      background-color: ${text1};
      height: 100vh;
      .content {
        ${flex("center", "center", "column")};
        height: 100vh;
        display: flex;
        p {
          font-size: 50px;
          font-weight: bold;
          color: ${text4};
          position: relative;
          width: 100%;
          text-align: center;
          &:before {
            content: "";
            position: absolute;
            left: 0;
            height: 10px;
            width: 45%;
            top: 50%;
            background-color: ${primary2};
          }
        }
        h1 {
          font-size: 22px;
          font-weight: bold;
          color: ${text4};
          margin-bottom: 32px;
        }
      }
    `)
);
